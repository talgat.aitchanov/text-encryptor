package com.icefire.controller;

import com.icefire.enums.EncryptionAlgorithmEnum;
import com.icefire.exception.AlgorithmException;
import com.icefire.exception.TextEncryptorException;
import com.icefire.model.EncryptedDataDTO;
import com.icefire.service.DecryptorService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DecryptorController implements BaseRestController {

    @Autowired
    private DecryptorService decryptorService;

    @GetMapping("/decrypt")
    public @ResponseBody
    EncryptedDataDTO decrypt(
        @RequestParam(value = "key") String key,
        @RequestParam(value = "encryptedValue") String encryptedValue
    ) throws AlgorithmException {

        if (StringUtils.isBlank(key)) {
            throw new TextEncryptorException("Please, set \"key\"");
        }
        if (StringUtils.isBlank(encryptedValue)) {
            throw new TextEncryptorException("Please, set \"encryptedValue\"");
        }

        boolean doesAlgorithmExist = doesAlgorithmExist(key);
        if (!doesAlgorithmExist) {
            throw new TextEncryptorException("No such algorithm found, key = " + key);
        }

        EncryptionAlgorithmEnum encryptionAlgorithmEnum = convertKeyStringToEnum(key);
        return decryptorService.decrypt(encryptionAlgorithmEnum, encryptedValue);
    }

}
