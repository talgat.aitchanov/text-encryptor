package com.icefire.controller;

import com.icefire.enums.EncryptionAlgorithmEnum;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.stream.Stream;

@RequestMapping("/rest")
public interface BaseRestController {

    default boolean doesAlgorithmExist(String key) {
        return Stream.of(EncryptionAlgorithmEnum.values()).anyMatch(ea -> ea.getValue().equals(key));
    }

    default EncryptionAlgorithmEnum convertKeyStringToEnum(String key) {
        return EncryptionAlgorithmEnum.valueOf(key);
    }

}
