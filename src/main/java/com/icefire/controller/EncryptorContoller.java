package com.icefire.controller;

import com.icefire.enums.EncryptionAlgorithmEnum;
import com.icefire.exception.AlgorithmException;
import com.icefire.exception.TextEncryptorException;
import com.icefire.model.EncryptedDataDTO;
import com.icefire.service.EncryptorService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class EncryptorContoller implements BaseRestController {

    @Autowired
    private EncryptorService encryptorService;

    @PostMapping("/encrypt")
    public @ResponseBody
    EncryptedDataDTO encrypt(
        @RequestParam(value = "key") String key,
        @RequestParam(value = "text") String text
    ) throws AlgorithmException {

        if (StringUtils.isBlank(key)) {
            log.error("Please, set \"key\"");
            throw new TextEncryptorException("Please, set \"key\"");
        }
        if (StringUtils.isBlank(text)) {
            log.error("Please, set \"text\"");
            throw new TextEncryptorException("Please, set \"text\"");
        }

        boolean doesAlgorithmExist = doesAlgorithmExist(key);
        if (!doesAlgorithmExist) {
            log.error("No such algorithm found, key = " + key);
            throw new TextEncryptorException("No such algorithm found, key = " + key);
        }

        EncryptionAlgorithmEnum encryptionAlgorithmEnum = convertKeyStringToEnum(key);
        return encryptorService.encrypt(encryptionAlgorithmEnum, text);
    }

}
