package com.icefire.algorithm;

import com.icefire.exception.AlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public abstract class Algorithm {

    public String encrypt(String crealText) throws AlgorithmException {
        String strData;
        try {
            SecretKeySpec skeyspec = new SecretKeySpec(getEncryptionKey().getBytes(), getAlgorithm());
            Cipher cipher = Cipher.getInstance(getCipherTransformation());
            cipher.init(Cipher.ENCRYPT_MODE, skeyspec);
            byte[] encrypted = cipher.doFinal(crealText.getBytes());
            strData = new String(encrypted);
        } catch (Exception e) {
            throw new AlgorithmException(e);
        }
        return strData;
    }

    public String decrypt(String encrypted) throws AlgorithmException {
        String strData;
        try {
            SecretKeySpec skeyspec = new SecretKeySpec(getEncryptionKey().getBytes(), getAlgorithm());
            Cipher cipher = Cipher.getInstance(getCipherTransformation());
            cipher.init(Cipher.DECRYPT_MODE, skeyspec);
            byte[] decrypted = cipher.doFinal(encrypted.getBytes());
            strData = new String(decrypted);
        } catch (Exception e) {
            throw new AlgorithmException(e);
        }
        return strData;
    }

    public abstract String getEncryptionKey();

    public abstract String getAlgorithm();

    public abstract String getCipherTransformation();

}
