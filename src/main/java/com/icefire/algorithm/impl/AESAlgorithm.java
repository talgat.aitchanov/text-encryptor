package com.icefire.algorithm.impl;

import com.icefire.algorithm.Algorithm;

public class AESAlgorithm extends Algorithm {

    private static final String encryptionKey = "ABCDEFGHIJKLMNOP";
    private static final String aesEncryptionAlgorithm = "AES";
    private static final String cipherTransformation = "AES/CBC/PKCS5PADDING";

    private static AESAlgorithm aESAlgorithm;

    private AESAlgorithm() {
    }

    public static Algorithm getInstance() {
        synchronized (AESAlgorithm.class) {
            if (aESAlgorithm == null) {
                aESAlgorithm = new AESAlgorithm();
            }
        }
        return aESAlgorithm;
    }

    @Override
    public String getEncryptionKey() {
        return encryptionKey;
    }

    @Override
    public String getAlgorithm() {
        return aesEncryptionAlgorithm;
    }

    @Override
    public String getCipherTransformation() {
        return cipherTransformation;
    }
}
