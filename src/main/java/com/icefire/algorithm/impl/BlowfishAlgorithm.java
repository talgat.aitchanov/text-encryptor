package com.icefire.algorithm.impl;

import com.icefire.algorithm.Algorithm;

public class BlowfishAlgorithm extends Algorithm {

    private static final String encryptionKey = "QRSTUVWXYZ";
    private static final String blowfishEncryptionAlgorithm = "Blowfish";
    private static final String cipherTransformation = "Blowfish";

    private static BlowfishAlgorithm blowfishAlgorithm;

    private BlowfishAlgorithm() {
    }

    public static Algorithm getInstance() {
        synchronized (BlowfishAlgorithm.class) {
            if (blowfishAlgorithm == null) {
                blowfishAlgorithm = new BlowfishAlgorithm();
            }
        }
        return blowfishAlgorithm;
    }

    @Override
    public String getEncryptionKey() {
        return encryptionKey;
    }

    @Override
    public String getAlgorithm() {
        return blowfishEncryptionAlgorithm;
    }

    @Override
    public String getCipherTransformation() {
        return cipherTransformation;
    }
}
