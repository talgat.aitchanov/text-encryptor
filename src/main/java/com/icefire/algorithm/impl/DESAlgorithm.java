package com.icefire.algorithm.impl;

import com.icefire.algorithm.Algorithm;

public class DESAlgorithm extends Algorithm {

    private static final String encryptionKey = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String cipherTransformation = "DES/ECB/PKCS5Padding";
    private static final String desEncryptionAlgorithm = "DES";

    private static DESAlgorithm dESAlgorithm;

    private DESAlgorithm() {
    }

    public static Algorithm getInstance() {
        synchronized (DESAlgorithm.class) {
            if (dESAlgorithm == null) {
                dESAlgorithm = new DESAlgorithm();
            }
        }
        return dESAlgorithm;
    }

    @Override
    public String getEncryptionKey() {
        return encryptionKey;
    }

    @Override
    public String getAlgorithm() {
        return desEncryptionAlgorithm;
    }

    @Override
    public String getCipherTransformation() {
        return cipherTransformation;
    }
}
