package com.icefire.service;

import com.icefire.algorithm.Algorithm;
import com.icefire.enums.EncryptionAlgorithmEnum;
import com.icefire.exception.AlgorithmException;
import com.icefire.factory.AlgorithmFactory;
import com.icefire.model.EncryptedDataDTO;
import com.icefire.model.entity.EncryptedData;
import com.icefire.repository.EncryptedKeyValueRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static com.icefire.util.Constant.EMPTY_STRING;

@Slf4j
@Service
public class EncryptorService {

    private final EncryptedKeyValueRepository encryptedKeyValueRepository;

    public EncryptorService(EncryptedKeyValueRepository encryptedKeyValueRepository) {
        this.encryptedKeyValueRepository = encryptedKeyValueRepository;
    }

    public EncryptedDataDTO encrypt(EncryptionAlgorithmEnum encryptionAlgorithmEnum, String text) throws AlgorithmException {

        EncryptedData encryptedData = retrieveFromBD(encryptionAlgorithmEnum, text);

        String encryptedValue = EMPTY_STRING;

        if (ObjectUtils.allNotNull(encryptedData)) {
            encryptedValue = getEncryptedValue(encryptedData);
            log.info("Got encryptedValue = {}", encryptedValue);
        }

        if (StringUtils.isEmpty(encryptedValue)) {
            Algorithm algorithm = AlgorithmFactory.getAlgorithm(encryptionAlgorithmEnum);

            log.info("Generating encryptedValue using {}", algorithm.getAlgorithm());
            encryptedValue = algorithm.encrypt(text);
            log.info("Generated encryptedValue = {}", encryptedValue);

            saveIntoDB(encryptionAlgorithmEnum, text, encryptedValue);
            log.info("Saved in DB");
        }

        EncryptedDataDTO encryptedDataDTO = buildEncryptedDTO(encryptionAlgorithmEnum.getValue(), text, encryptedValue);

        log.info("{}", encryptedDataDTO);

        return encryptedDataDTO;
    }

    private EncryptedData retrieveFromBD(EncryptionAlgorithmEnum encryptionAlgorithmEnum, String text) {
        return encryptedKeyValueRepository.findFirstByKeyAndText(encryptionAlgorithmEnum.getValue(), text);
    }

    private String getEncryptedValue(EncryptedData encryptedData) {
        return encryptedData.getEncryptedValue();
    }

    private void saveIntoDB(EncryptionAlgorithmEnum encryptionAlgorithmEnum, String text, String encryptedValue) {
        EncryptedData encryptedData = EncryptedData.builder()
            .key(encryptionAlgorithmEnum.getValue())
            .text(text)
            .encryptedValue(encryptedValue)
            .build();
        encryptedKeyValueRepository.save(encryptedData);
    }

    private EncryptedDataDTO buildEncryptedDTO(String encryptionAlgorithm, String text, String encryptedValue) {
        return EncryptedDataDTO.builder()
            .key(encryptionAlgorithm)
            .originalText(text)
            .encryptedValue(encryptedValue)
            .build();
    }

}
