package com.icefire.service;

import com.icefire.algorithm.Algorithm;
import com.icefire.enums.EncryptionAlgorithmEnum;
import com.icefire.exception.AlgorithmException;
import com.icefire.factory.AlgorithmFactory;
import com.icefire.model.EncryptedDataDTO;
import com.icefire.model.entity.EncryptedData;
import com.icefire.repository.EncryptedKeyValueRepository;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static com.icefire.util.Constant.EMPTY_STRING;

@Service
public class DecryptorService {

    private final EncryptedKeyValueRepository encryptedKeyValueRepository;

    public DecryptorService(EncryptedKeyValueRepository encryptedKeyValueRepository) {
        this.encryptedKeyValueRepository = encryptedKeyValueRepository;
    }

    public EncryptedDataDTO decrypt(EncryptionAlgorithmEnum encryptionAlgorithmEnum, String encryptedValue) throws AlgorithmException {
        EncryptedData encryptedData = retrieveFromBD(encryptionAlgorithmEnum, encryptedValue);

        String text = getText(encryptedData);

        if (StringUtils.isEmpty(text)) {
            Algorithm algorithm = AlgorithmFactory.getAlgorithm(encryptionAlgorithmEnum);
            text = algorithm.decrypt(encryptedValue);
            saveIntoDB(encryptionAlgorithmEnum, text, encryptedValue);
        }

        return EncryptedDataDTO.builder()
            .key(encryptionAlgorithmEnum.getValue())
            .originalText(text)
            .encryptedValue(encryptedValue)
            .build();
    }

    private EncryptedData retrieveFromBD(EncryptionAlgorithmEnum encryptionAlgorithmEnum, String encryptedValue) {
        return encryptedKeyValueRepository.findFirstByKeyAndAndEncryptedValue(encryptionAlgorithmEnum.getValue(), encryptedValue);
    }

    private String getText(EncryptedData encryptedData) {
        String retVal = EMPTY_STRING;
        if (ObjectUtils.allNotNull(encryptedData)) {
            retVal = encryptedData.getText();
        }
        return retVal;
    }

    private void saveIntoDB(EncryptionAlgorithmEnum encryptionAlgorithmEnum, String text, String encryptedValue) {
        EncryptedData encryptedData = EncryptedData.builder()
            .key(encryptionAlgorithmEnum.getValue())
            .text(text)
            .encryptedValue(encryptedValue)
            .build();
        encryptedKeyValueRepository.save(encryptedData);
    }

}
