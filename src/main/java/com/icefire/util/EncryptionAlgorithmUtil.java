package com.icefire.util;

import com.icefire.enums.EncryptionAlgorithmEnum;

public class EncryptionAlgorithmUtil {

    public static EncryptionAlgorithmEnum getEncryptionAlgorithmEnum(String key) {
        if (EncryptionAlgorithmEnum.AES.getValue().equals(key)) {
            return EncryptionAlgorithmEnum.AES;
        } else {
            return EncryptionAlgorithmEnum.DES;
        }
    }

}
