package com.icefire.util;

import com.icefire.model.entity.EncryptedData;

import static com.icefire.util.Constant.EMPTY_STRING;

public class EncryptedKeyValueUtil {

    public static EncryptedData buildEmptyEncrypedtKeyValue() {
        return EncryptedData.builder()
            .key(EMPTY_STRING)
            .encryptedValue(EMPTY_STRING)
            .build();
    }

}
