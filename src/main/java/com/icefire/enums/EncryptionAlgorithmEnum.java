package com.icefire.enums;

public enum EncryptionAlgorithmEnum {

    AES("AES"),
    DES("DES"),
    BLOWFISH("BLOWFISH");

    private String value;

    EncryptionAlgorithmEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
