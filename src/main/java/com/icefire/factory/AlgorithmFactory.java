package com.icefire.factory;

import com.icefire.algorithm.Algorithm;
import com.icefire.algorithm.impl.AESAlgorithm;
import com.icefire.algorithm.impl.BlowfishAlgorithm;
import com.icefire.algorithm.impl.DESAlgorithm;
import com.icefire.enums.EncryptionAlgorithmEnum;
import com.icefire.exception.AlgorithmException;

public class AlgorithmFactory {

    public static Algorithm getAlgorithm(EncryptionAlgorithmEnum key) throws AlgorithmException {
        if (EncryptionAlgorithmEnum.AES == key) {
            return AESAlgorithm.getInstance();
        } else if (EncryptionAlgorithmEnum.DES == key) {
            return DESAlgorithm.getInstance();
        } else if (EncryptionAlgorithmEnum.BLOWFISH == key) {
            return BlowfishAlgorithm.getInstance();
        } else {
            throw new AlgorithmException("No such algorithm");
        }
    }

}
