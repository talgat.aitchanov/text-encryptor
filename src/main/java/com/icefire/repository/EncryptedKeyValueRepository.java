package com.icefire.repository;

import com.icefire.model.entity.EncryptedData;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EncryptedKeyValueRepository extends JpaRepository<EncryptedData, Long> {

    EncryptedData findFirstByKeyAndText(String key, String text);

    EncryptedData findFirstByKeyAndAndEncryptedValue(String key, String encryptedValue);

}
