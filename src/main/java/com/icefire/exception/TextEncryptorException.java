package com.icefire.exception;

public class TextEncryptorException extends RuntimeException {

    public TextEncryptorException(String message) {
        super(message);
    }

    public TextEncryptorException(String message, Throwable cause) {
        super(message, cause);
    }

    public TextEncryptorException(Throwable cause) {
        super(cause);
    }
}
