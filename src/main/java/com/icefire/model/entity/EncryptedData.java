package com.icefire.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "encrypted_data")
public class EncryptedData implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "key")
    @EqualsAndHashCode.Include
    private String key;

    @Column(name = "text")
    @EqualsAndHashCode.Include
    private String text;

    @Column(name = "encryptedValue")
    @EqualsAndHashCode.Include
    private String encryptedValue;

    @Override
    public String toString() {
        return "Id: " + id
            + "Key: " + key
            + "Text: " + text
            + " EncryptedValue: " + encryptedValue
            ;
    }


}
