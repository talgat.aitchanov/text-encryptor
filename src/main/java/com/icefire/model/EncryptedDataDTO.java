package com.icefire.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
@ToString
public class EncryptedDataDTO {

    private String key;
    private String originalText;
    private String encryptedValue;

}
