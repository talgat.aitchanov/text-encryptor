package com.icefire.controller;

import com.icefire.enums.EncryptionAlgorithmEnum;
import com.icefire.model.EncryptedDataDTO;
import com.icefire.service.DecryptorService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DecryptorController.class)
public class DecryptorControllerTest {

    private EncryptedDataDTO encryptedDataDTO;
    private String key;
    private EncryptionAlgorithmEnum keyEnum;
    private String text;
    private String encryptedValue;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DecryptorService decryptorService;

    @Before
    public void setUp() {
        key = EncryptionAlgorithmEnum.AES.getValue();
        keyEnum = EncryptionAlgorithmEnum.AES;
        text = "test";
        encryptedValue = "sadsadw2121";

        encryptedDataDTO = new EncryptedDataDTO();
        encryptedDataDTO.setKey(key);
        encryptedDataDTO.setOriginalText(text);
        encryptedDataDTO.setEncryptedValue(encryptedValue);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void decryptTestUnauthorized() throws Exception {

        given(decryptorService.decrypt(keyEnum, encryptedValue)).willReturn(encryptedDataDTO);

        this.mvc.perform(get("/rest/decrypt")
            .contentType(MediaType.APPLICATION_JSON)
            .param("key", key)
            .param("encryptedValue", encryptedValue))
            .andDo(print())
            .andExpect(status().isUnauthorized())
        ;
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    public void decryptTestOk() throws Exception {

        given(decryptorService.decrypt(keyEnum, encryptedValue)).willReturn(encryptedDataDTO);

        this.mvc.perform(get("/rest/decrypt")
            .contentType(MediaType.APPLICATION_JSON)
            .param("key", key)
            .param("encryptedValue", encryptedValue))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string("{\"key\":\"AES\",\"originalText\":\"test\",\"encryptedValue\":\"sadsadw2121\"}"))
        ;
    }
}